﻿using UnityEngine;
using System.Collections.Generic;

public class LightManager<T> : MonoBehaviour
{
    protected static LightManager<T> s_Instance;
    protected HashSet<T> m_Container = new HashSet<T>();
    protected static bool initialized = false;


    protected void Awake(){var init = Instance;}
    protected void Start() { var init = Instance; }
    protected void OnEnable() { var init = Instance; }


    public static LightManager<T> Instance
    {
        get
        {
            if (initialized)
            {
                return s_Instance;
            }
            else
            {
                s_Instance = (LightManager<T>)FindObjectOfType<LightManager<T> >();
                if (s_Instance!=null){initialized = true;}
                return s_Instance;
            }
        }
    }


    private static LightManager< T > containerCopy;
    public static HashSet<T> Get()
    {
        containerCopy = Instance;
        if (initialized)
        {
            return containerCopy.m_Container;
        }
        else
        {
            return new HashSet<T>();
        }
    }


    private static LightManager<T> containerCopyToIncrement;
    public static bool Add(T t)
    {
        containerCopyToIncrement = Instance;
        containerCopyToIncrement.m_Container.Add(t);
        return true;
    }


    private static LightManager< T > containerCopyToDecrement;
    public static void Remove(T t)
    {
        containerCopyToDecrement = Instance;
        containerCopyToDecrement.m_Container.Remove(t);
    }
}
