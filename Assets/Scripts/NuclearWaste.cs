using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedBehaviours;

public sealed class NuclearWaste : MyMonoBehaviour
{
    public enum ParticlesCullingMode
    {
        VISIBLE_FROM_CAMERA,
        DISTANCE_FROM_SUBMARINE
    };

    [SerializeField] private float cullingRadius = 45;
    [SerializeField] private MonoBehaviour[] volumetricLights;
    private ParticleSystem[] itsParticleSystems = null;
    private int amountOfParticles;
    private Camera mainCam;
    public MeshRenderer cullingSphere;

    [SerializeField] private bool readyToProcessCulling = true;
    [SerializeField] private ParticlesCullingMode cullingMode = ParticlesCullingMode.VISIBLE_FROM_CAMERA;
    Transform submarineTransform;

    private IEnumerator Start()
    {
        Initialize();

        yield return new WaitWhile(() => mainViewport == null);

        itsParticleSystems = GetComponentsInChildren<ParticleSystem>();

        if (itsParticleSystems == null || itsParticleSystems.Length <= 0)
        {
            Debug.Log("[HANDS-UP!] NuclearWaste behaviour did not found any particles to calculate culling. It will be disabled.");

            enabled = false;
            yield return null;
        }

        amountOfParticles = itsParticleSystems.Length;
        submarineTransform = Protagonist.protagonistInstance.itsTransform;

        mainCam = mainViewport.GetComponent<Camera>();

        if (cullingMode == ParticlesCullingMode.VISIBLE_FROM_CAMERA)
        {
            readyToProcessCulling = !RendererHelper.IsVisibleFrom(cullingSphere, mainCam);
            GameManager.AfterSlowLateUpdate += NuclearWaste_ProcVisibleFromCamera;
        }
        else
        {
            float dist = (submarineTransform.position - itsTransform.position).magnitude;
            bool isNear = dist <= cullingRadius;
            readyToProcessCulling = !isNear;

            GameManager.AfterSlowLateUpdate += NuclearWaste_ProcDistFromSubmarine;
        }

        yield return new WaitForSeconds(3);

        foreach (var light in volumetricLights)
        {
            light.enabled = true;
        }
    }

    void NuclearWaste_ProcVisibleFromCamera()
    {
        if (itsRigidbody.IsSleeping())
        {
            bool isVisible = RendererHelper.IsVisibleFrom(cullingSphere, mainCam);

            if (isVisible && !readyToProcessCulling)
            {
                for (int i = 0; i < amountOfParticles; i++)
                {
                    ParticleSystem ps = itsParticleSystems[i];
                    //TODO criar unity objects � cada frame gera garbage collection, este campo n�o precisa de uma refer�ncia porque o main m�dule possui vario�veis que podem ser modificadas atrav�s de geters e seters, enquanto outras vari�veis mais complexas precisam substituir completamente o particle module porque s�o readonly
                    if (!ps.isPlaying) ps.Play();
                    ParticleSystem.MainModule mm = ps.main;
                    mm.simulationSpeed = 2.0f;
                    mm.loop = true;
                }

                readyToProcessCulling = true;
            }
            else if (!isVisible && readyToProcessCulling)
            {
                for (int i = 0; i < amountOfParticles; i++)
                {
                    ParticleSystem ps = itsParticleSystems[i];
                    if (ps.isPlaying) ps.Stop();
                }

                readyToProcessCulling = false;
            }
        }
        else
        {
            ParticlesSimulation_WhenBodyIsAwake();
        }
    }

    void NuclearWaste_ProcDistFromSubmarine()
    {
        if (itsRigidbody.IsSleeping())
        {
            float dist = (submarineTransform.localPosition - itsTransform.localPosition).magnitude;
            bool isNear = dist <= cullingRadius;

            if (isNear && !readyToProcessCulling)
            {
                for (int i = 0; i < amountOfParticles; i++)
                {
                    ParticleSystem ps = itsParticleSystems[i];
                    if (!ps.isPlaying) ps.Play();

                    ParticleSystem.MainModule mm = ps.main;
                    mm.simulationSpeed = 2.0f;
                    mm.loop = true;
                }

                readyToProcessCulling = true;
            }
            else if (!isNear && readyToProcessCulling)
            {
                for (int i = 0; i < amountOfParticles; i++)
                {
                    ParticleSystem ps = itsParticleSystems[i];
                    if (ps.isPlaying) ps.Stop();
                }

                readyToProcessCulling = false;
            }
        }
        else
        {
            ParticlesSimulation_WhenBodyIsAwake();
        }
    }

    void ParticlesSimulation_WhenBodyIsAwake()
    {
        for (int i = 0; i < amountOfParticles; i++)
        {
            ParticleSystem ps = itsParticleSystems[i];
            if (!ps.isPlaying) ps.Play();
            ParticleSystem.MainModule mm = ps.main;
            mm.simulationSpeed = 6.0f;
            mm.loop = false;
        }
    }

    private void OnDisable()
    {
        if (cullingMode == ParticlesCullingMode.VISIBLE_FROM_CAMERA)
        {
            GameManager.AfterSlowLateUpdate -= NuclearWaste_ProcVisibleFromCamera;
        }
        else
        {
            GameManager.AfterSlowLateUpdate -= NuclearWaste_ProcDistFromSubmarine;
        }
    }
}